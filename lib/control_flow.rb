# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  str.each_char do |ch|
    str.delete!(ch) if ch.downcase == ch
  end
  str
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  middle = str.length/2
  return str[middle] if str.length.odd?
  str[middle-1..middle]
end

# Return the number of vowels in a string.
def num_vowels(str)
  str.downcase.count("aeiou")
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  (1..num).to_a.reduce(:*)
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = '')
  arr.each_with_index.reduce('') do |acc, (el, idx)|
    idx == arr.length - 1 ? acc << el : acc << el + separator
  end
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  str.chars.map.with_index do |ch, i|
    i.even? ? ch.downcase : ch.upcase
  end.join
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  str.split.map do |word|
    word.length >= 5 ? word.reverse : word
  end.join(' ')
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  (1..n).map do |int|
    if (int % 3 == 0) && (int % 5 == 0)
      'fizzbuzz'
    elsif int % 3 == 0
      'fizz'
    elsif int % 5 == 0
      'buzz'
    else
      int
    end
  end
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  newarr = []
  arr.each { |el| newarr.unshift(el) }
  newarr
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  return false if [0, 1].include?(num)
  (2..num/2).to_a.none? { |n| num % n == 0 }
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  (1..num).to_a.map do |n|
    n if num % n == 0
  end.compact
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  factors(num).select{ |n| prime?(n) }
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).length
end

# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  odds = arr.select(&:odd?)
  evens = arr.select(&:even?)

  odds.length < evens.length ? odds.first : evens.first
end
